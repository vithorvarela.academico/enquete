from django.db import models
from django.utils import timezone
# Create your models here.

# every blog post has a category 
class Category(models.Model):
    name = models.CharField(max_length = 125)
    description = models.TextField(max_length= 255)
    def __str__(self):
        return self.name  

# author of a post 
class Author(models.Model):
    name = models.CharField(max_length = 125)
    age = models.IntegerField()
    description = models.TextField(max_length= 255)
    def __str__(self):
        return self.name

# post of the blog 
class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    category = models.ManyToManyField(Category)
    title = models.CharField(max_length=255)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


