from django.shortcuts import render
from .models import Post
from django.shortcuts import render, get_object_or_404

# Create your views here.
def index(request):
    all_posts = Post.objects.all()
    return render(request, 'devblog/index.html', {'posts': all_posts})

def about(request):
    return render(request, 'devblog/about.html')

def posts(request, post_id): 
    post = Post.objects.get(id=post_id)
    return render(request, 'devblog/blog-post.html', {'post': post})
