from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'devblog'

urlpatterns = [

    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('posts/<int:post_id>', views.posts, name ='posts'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)