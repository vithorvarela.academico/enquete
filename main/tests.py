import datetime
from django.utils import timezone
from django.test import TestCase
from main.models import Pergunta
from django.urls import reverse

# Create your tests here.

class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        """
        o método publicada recentemente precisa retornar FALSE quando se tratar
        de perguntas com data de publicação no futuro.
        """
        data_futura = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura  = Pergunta(data_publicacao = data_futura)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24h_no_passado(self):

        """
        o método publicada recentemente DEVE retornar False quando se tratar
        de uma data de publicação antreior a 24hs no passado.
        """

        data_passada = timezone.now() + datetime.timedelta(days = 1, seconds=1)
        pergunta_passado = Pergunta(data_publicacao = data_passada)
        self.assertIs(pergunta_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hs(self):

        """
        o método publicada_recentemente DEVE retornar TRUE quando se tratar de
        uma data de publicação das últimas 24hrs.
        """
        data = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        pergunta = Pergunta(data_publicacao = data)
        self.assertIs(pergunta.publicada_recentemente(), True)

def criar_pergunta(texto, dias):
    """
    função para criação de uma pergunta para texto e data de publicação
    """
    data = timezone.now() + datetime.timedelta(days = dias)
    return Pergunta.objects.create(texto = texto, data_publicacao = data)

class IndexViewTeste(TestCase):

    def test_sem_perguntas_cadastradas(self):
        """
        Teste da IndexView quando não houverem perguntas cadastradas.
        """
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], [])

    def test_com_pergunta_no_passado(self):
        """
        Teste da IndexView exibindo normalmente pergunta no passado.
        """
        "criar_pergunta(texto='Pergunta no passado', dias = -30)"
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        "self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], ['<Pergunta: Pergunta no passado>'])"

    def test_com_pergunta_no_futuro(self):
        "criar_pergunta(texto=Pergunta no futuro, dias = 1)"
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "não há enquetes cadastradas até o momento!")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], [])


    def test_duas_perguntas_no_passado(self):
        "criar_pergunta(texto='Pergunta no passado 1', dias = -1)"
        "criar_pergunta(texto='Pergunta no passado 2', dias = -1)"
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        "self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], ['<Pergunta: Pergunta no passado>'])"

